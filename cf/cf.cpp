//Written by Chansol, Team Directors.

#pragma comment (lib, "Wininet.lib")

#include <stdio.h>
#include <iostream>
#include <list>
#include <map>
#include <vector>
#include <queue>
#include "cMySQL.h"
#include <wininet.h>
#include <time.h>
#include <math.h>

#define ROUNDING(x, dig)    (floor((x) * pow(10, dig) + 0.5f) / pow(10, dig))

using namespace std;

class Matrix
{
public:
	Matrix(size_t rows, size_t cols);
	double& operator()(size_t i, size_t j);
	double operator()(size_t i, size_t j) const;

private:
	size_t mRows;
	size_t mCols;
	std::vector<double> mData;
};

Matrix::Matrix(size_t rows, size_t cols)
: mRows(rows),
mCols(cols),
mData(rows * cols)
{
}

double& Matrix::operator()(size_t i, size_t j)
{
	return mData[i * mCols + j];
}

double Matrix::operator()(size_t i, size_t j) const
{
	return mData[i * mCols + j];
}

struct compare
{
	bool operator()(pair<int, double> v1, pair<int, double> v2)
	{
		return v1.second > v2.second;
	}
};

map<int, list<pair<int, double>>> mapUser;
map<int, int> real_index; //관리, 실제

cMySQL db;

int running_state = 0;


void ReadCSV(const char *path)
{
	FILE *fp = fopen(path, "r");
	if (fp == nullptr)
	{
		throw exception("Cannot open CSV file");
	}

	while (!feof(fp))
	{
		int v1, v2;
		double v3;

		fscanf(fp, "%d,%d,%lf", &v1, &v2, &v3);

		auto &it = mapUser.find(v1);
		if (it == mapUser.end())
		{
			list<pair<int, double>> listSong;
			mapUser.insert(pair<int, list<pair<int, double>>>(v1, listSong));

			it = mapUser.find(v1);
		}

		list<pair<int, double>> &listSong = it->second;

		listSong.push_back(pair<int, double>(v2, v3));
	}
}

void ReadDB()
{
	int cntid = -1;
	int lastid = -1;

	db.Query("SELECT ruidx, rmidx, rmusic_rating, risplaylist, rplay_count FROM rating ORDER BY ruidx");
	while (db.FetchRow() != -1)
	{
		int v1, v2;
		double v3, v4, v5;

		v1 = db.GetInt(0); //user index
		v2 = db.GetInt(1); //music index

		v3 = db.GetDouble(2); //rating
		v4 = db.GetDouble(3); //playlist
		v5 = db.GetDouble(4); //play count

		v4 = v4 * 5; //0 or 5

		if (v5 > 200) v5 = 200;
		v5 = (v5 / 200) * 5;

		if (lastid != v1)
		{
			lastid = v1;
			real_index.insert(pair<int, int>(++cntid, v1));
			v1 = cntid;
		}
		else
		{
			v1 = cntid;
		}

		//printf("%d %d %lf %lf %lf\n", v1, v2, v3, v4, v5);

		auto &it = mapUser.find(v1);
		if (it == mapUser.end())
		{
			list<pair<int, double>> listSong;
			mapUser.insert(pair<int, list<pair<int, double>>>(v1, listSong));

			it = mapUser.find(v1);
		}

		list<pair<int, double>> &listSong = it->second;

		//가중치에 따른 예상 별점 계산
		double scroesum = ((v3 * 0.85) + (v4 * 0.05) + (v5 * 0.1));

		//곡번호, 점수를 넣는다
		listSong.push_back(pair<int, double>(v2, scroesum));
	}
}

double avgUser(int keyUser)
{
	int cnt = 0;
	double sum = 0.0;
	
	auto user = mapUser.find(keyUser);
	if (user == mapUser.end())
	{
		return 0.0;
	}
	else
	{
		for (auto item : user->second)
		{
			++cnt;
			sum += item.second;
		}
		return sum / (double)cnt;
	}
}

void getPredict(Matrix &m, int msize, int keyUser, int k, FILE *fp, FILE *fp2)
{
	priority_queue<pair<int, double>, vector<pair<int, double>>, compare > knn;

	for (int i = 0; i <= msize; ++i)
	{
		if (i == keyUser) continue;
		
		if (knn.size() >= k)
		{
			if (knn.top().second < m(keyUser, i))
			{
				knn.pop();
			}
			else
			{
				continue;
			}
		}

		knn.push(pair<int, double>(i, m(keyUser, i)));
	}

	int knn_user; //1nn user
	double knn_simul; //1nn 유사도

	double avgx = avgUser(keyUser); //내 평균 점수
	//pair<double, double> 총유사도, 합산점수
	map<int, pair<double, double>> mapScore;
	while (!knn.empty())
	{
		knn_user = knn.top().first;
		knn_simul = knn.top().second;

		double avgy = avgUser(knn.top().first); //상대 평균 점수

		auto user = mapUser.find(knn.top().first);

		for (auto item : user->second)
		{
			auto &score = mapScore.find(item.first);
			if (score == mapScore.end())
			{
				mapScore.insert(pair<int, pair<double, double>>(item.first, pair<double, double>(0.0, 0.0)));
				score = mapScore.find(item.first);
			}
			//else
			//{
				score->second.first = score->second.first + knn.top().second; //유사도함

				if (user->first < 1000) //진짜 유저와 DJ로그를 구분하여 계산한다
				{
					avgy = 3.5;
				}
				score->second.second = score->second.second + ((item.second - avgy) * knn.top().second);

				/*if (user->first > 9003) //진짜 유저와 DJ로그를 구분하여 계산한다
				{
					score->second.second = score->second.second + ((item.second - avgy) * knn.top().second);
				}
				else
				{
					score->second.second = score->second.second + (item.second * knn.top().second);
				}*/
				/*if (keyUser == 9096)
				{
					printf("myavg: %lf\n", avgx);
				}*/
				/*auto ruser = real_index.find(keyUser);
				if (ruser->second == 9096 && item.first == 3153013 && item.second > 0.0)
				{
					printf("user: %d %lf\n", user->first, knn.top().second);
					printf("myavg: %lf\n", avgx);
					printf("target: %lf %lf\n\n", avgy, item.second);
				}*/
				//printf("%lf %lf\n", score->second.first, score->second.second);
			//}
		}

		knn.pop();
	}

	auto user = real_index.find(keyUser);
	for (auto item : mapScore)
	{

		int flag = 0; //내가 평가한 곡인지 체크한다
		auto usermap = mapUser.find(keyUser);
		for (auto music : usermap->second)
		{
			if (item.first == music.first)
			{
				flag = 1;
				break;
			}
		}
		if (flag) continue;

		if (item.second.first > 0.0)
		{
			double predict_score = avgx + (item.second.second / item.second.first);

			if (predict_score > 5.0)
			{
				predict_score = 5.0;
			}
			else if (predict_score < 0.5)
			{
				predict_score = 0.5;
			}

			predict_score = ROUNDING(predict_score, 2);

			fprintf(fp, "%d,%d,%lf\n", user->second, item.first, predict_score);

			

			//fprintf(fp, "%d,%d,%lf\n", user->second, item.first, avgx + (item.second.second / item.second.first));
		}

	}

	auto knn_realuser = real_index.find(knn_user);
	fprintf(fp2, "%d,%d,%lf\n", user->second, knn_realuser->second, knn_simul); //가장 유사한 사람

}

void UploadCSV(const char *filename)
{
	// Local variables
	//char *filename = "C:/ssong/recommend.csv";   //Filename to be loaded
	char *type = "image/jpg";
	char boundary[] = "myssongcf";            //Header boundary
	char nameForm[] = "filepath";     //Input form name
	//char iaddr[] = "localhost";        //IP address
	char iaddr[] = "210.118.74.212";        //IP address
	char url[] = "SSong/UploadCSV.do";         //URL

	char hdrs[255];                  //Headers
	char * buffer;                   //Buffer containing file + headers
	char * content;                  //Buffer containing file
	FILE * pFile;                    //File pointer
	long lSize;                      //File size
	size_t result;

	// Open file
	pFile = fopen(filename, "rb");
	if (pFile == NULL) throw "ERROR_OPEN_FILE";

	// obtain file size:
	fseek(pFile, 0, SEEK_END);
	lSize = ftell(pFile);
	rewind(pFile);

	// allocate memory to contain the whole file:
	content = (char*)malloc(sizeof(char)*lSize);
	if (content == NULL) throw "ERROR_MEMORY";

	// copy the file into the buffer:
	result = fread(content, 1, lSize, pFile);
	if (result != lSize) throw "ERROR_SIZE";

	// terminate
	fclose(pFile);

	//allocate memory to contain the whole file + HEADER
	buffer = (char*)malloc(sizeof(char)*lSize + 2048);

	//print header
	sprintf(hdrs, "Content-Type: multipart/form-data; boundary=%s", boundary);
	sprintf(buffer, "--%s\r\nContent-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n", boundary, nameForm, filename);
	sprintf(buffer, "%sContent-Type: %s\r\n\r\n", buffer, type);
	sprintf(buffer, "%s%s\r\n", buffer, content);
	sprintf(buffer, "%s--%s--\r\n", buffer, boundary);
	
	//Open internet connection
	HINTERNET hSession = InternetOpen("WinSock", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
	if (hSession == NULL) throw "ERROR_INTERNET_OPEN";

	HINTERNET hConnect = InternetConnect(hSession, iaddr, 8123, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 1);
	if (hConnect == NULL) throw "ERROR_INTERNET_CONN";

	HINTERNET hRequest = HttpOpenRequest(hConnect, (const char*)"POST", url, NULL, NULL, (const char**)"*/*\0", 0, 1);
	if (hRequest == NULL) throw "ERROR_INTERNET_REQ";

	BOOL sent = HttpSendRequest(hRequest, hdrs, strlen(hdrs), buffer, strlen(buffer)); //why error?
	if (!sent) throw "ERROR_INTERNET_SEND";

	//close any valid internet-handles
	InternetCloseHandle(hSession);
	InternetCloseHandle(hConnect);
	InternetCloseHandle(hRequest);

	//free buffer
	free(content);
	free(buffer);
}

void UploadKNN(const char *filename)
{
	// Local variables
	//char *filename = "C:/ssong/recommend.csv";   //Filename to be loaded
	char *type = "image/jpg";
	char boundary[] = "myssongcf";            //Header boundary
	char nameForm[] = "filepath";     //Input form name
	//char iaddr[] = "localhost";        //IP address
	char iaddr[] = "210.118.74.212";        //IP address
	char url[] = "SSong/UploadKN1.do";         //URL

	char hdrs[255];                  //Headers
	char * buffer;                   //Buffer containing file + headers
	char * content;                  //Buffer containing file
	FILE * pFile;                    //File pointer
	long lSize;                      //File size
	size_t result;

	// Open file
	pFile = fopen(filename, "rb");
	if (pFile == NULL) throw "ERROR_OPEN_FILE";

	// obtain file size:
	fseek(pFile, 0, SEEK_END);
	lSize = ftell(pFile);
	rewind(pFile);

	// allocate memory to contain the whole file:
	content = (char*)malloc(sizeof(char)*lSize);
	if (content == NULL) throw "ERROR_MEMORY";

	// copy the file into the buffer:
	result = fread(content, 1, lSize, pFile);
	if (result != lSize) throw "ERROR_SIZE";

	// terminate
	fclose(pFile);

	//allocate memory to contain the whole file + HEADER
	buffer = (char*)malloc(sizeof(char)*lSize + 2048);

	//print header
	sprintf(hdrs, "Content-Type: multipart/form-data; boundary=%s", boundary);
	sprintf(buffer, "--%s\r\nContent-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n", boundary, nameForm, filename);
	sprintf(buffer, "%sContent-Type: %s\r\n\r\n", buffer, type);
	sprintf(buffer, "%s%s\r\n", buffer, content);
	sprintf(buffer, "%s--%s--\r\n", buffer, boundary);

	//Open internet connection
	HINTERNET hSession = InternetOpen("WinSock", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
	if (hSession == NULL) throw "ERROR_INTERNET_OPEN";

	HINTERNET hConnect = InternetConnect(hSession, iaddr, 8123, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 1);
	if (hConnect == NULL) throw "ERROR_INTERNET_CONN";

	HINTERNET hRequest = HttpOpenRequest(hConnect, (const char*)"POST", url, NULL, NULL, (const char**)"*/*\0", 0, 1);
	if (hRequest == NULL) throw "ERROR_INTERNET_REQ";

	BOOL sent = HttpSendRequest(hRequest, hdrs, strlen(hdrs), buffer, strlen(buffer)); //why error?
	if (!sent) throw "ERROR_INTERNET_SEND";

	//close any valid internet-handles
	InternetCloseHandle(hSession);
	InternetCloseHandle(hConnect);
	InternetCloseHandle(hRequest);

	//free buffer
	free(content);
	free(buffer);
}

BOOL CtrlHandler(DWORD dwCtrlType)
{
	switch (dwCtrlType)
	{
	case CTRL_C_EVENT:
		//case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
	case CTRL_CLOSE_EVENT:
		printf("info> wait until done..\n");
		running_state = 0;
	}
	return TRUE;
}

int main()
{
	//Test Code Upload
	//char* temp = "C:/ssong/recommend_1416843547.csv";
	//UploadCSV(temp);
	//return 0;

	SetConsoleTitle("cf processer");
	printf("################################################################################");
	printf("\n\n\n");
	printf("\t\t\t\t    My SSongs\n\n");
	printf("\t\t\tCollaborative Filtering Processer\n");
	printf("\t\t\t\tby Team Directors\n");
	printf("\n\n\n");
	printf("################################################################################");
	char dbpw[100];

	printf("Enter DB password : ");
	scanf("%s", dbpw);

	if (db.Connect("210.118.74.212", 3306, "root", dbpw, "SSong") == false)
	{
		printf("error> fail to connect db server\n");
		return 0;
	}

	system("cls");

	printf("info> db connected\n");

	Sleep(1000);
	printf("info> cf start\n\n");

	//init
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE);

	int cycle = 0;
	running_state = 1;

	while (running_state)
	{


		mapUser.clear();



		/*try
		{
		ReadCSV("C:/testdata/result.csv");
		}
		catch (exception &e)
		{
		printf("error> %s\n", e.what());
		}*/


		try
		{
			ReadDB();
		}
		catch (exception &e)
		{
			printf("error> %s\n", e.what());
		}

		printf("info> %d users loaded..\n", mapUser.size() + 1);

		printf("info> set matrix\n");
		Matrix m(mapUser.size() + 1, mapUser.size() + 1);

		printf("info> calculate similarity..\n");
		for (auto userx : mapUser)
		{
			//printf("info> processing %d user..\n", userx.first);
			for (auto usery : mapUser)
			{
				list<pair<int, double>> songx = userx.second;
				list<pair<int, double>> songy = usery.second;

				double fa = 0, fb = 0, fc = 0;

				for (auto scorex : songx)
				{
					for (auto scorey : songy)
					{
						if (scorex.first == scorey.first)
						{
							fa += scorex.second * scorey.second;
						}
					}
					fb += scorex.second * scorex.second;
				}
				for (auto scorey : songy)
				{
					fc += scorey.second * scorey.second;
				}

				double cs = 0; //cosine similarity
				if (fb && fc) //cannot divide by 0
				{
					cs = fa / (sqrt(fb) * sqrt(fc));
				}

				//printf("%d->%d: %f\n", userx.first, usery.first, cs);

				m(userx.first, usery.first) = cs;
			}
		}


		time_t timer = time(NULL);

		char csvpath[200];
		char knnpath[200];
		sprintf(csvpath, "C:/ssong/recommend_%u.csv", timer);
		sprintf(knnpath, "C:/ssong/knn_%u.csv", timer);

		printf("info> write %s..\n", csvpath);
		printf("info> write %s..\n", knnpath);
		FILE *fp = fopen(csvpath, "w");
		FILE *fp2 = fopen(knnpath, "w");

		printf("info> predict score..\n");
		for (auto user : mapUser)
		{
			//printf("info> predict %d user's score..\n", user.first);
			getPredict(m, mapUser.size(), user.first, 200, fp, fp2);
		}

		int filesize = ftell(fp);
		fclose(fp);

		int filesize2 = ftell(fp2); //유사도
		fclose(fp2);

		//upload
		printf("info> uploading csv file.. %dbytes\n", filesize);
		try
		{
			UploadCSV(csvpath);
		}
		catch (exception &e)
		{
			printf("error> %s\n", e.what());
		}

		printf("info> remove csv file\n");
		remove(csvpath);

		//upload knn
		printf("info> uploading knn file.. %dbytes\n", filesize2);
		try
		{
			UploadKNN(knnpath);
		}
		catch (exception &e)
		{
			printf("error> %s\n", e.what());
		}

		printf("info> remove knn file\n");
		remove(knnpath);

		printf("info> process done (cycle %d)\n\n", ++cycle);


		//wait
		int cnt_wait = 0;
		printf("info> wait 60 seconds\n");
		while (running_state)
		{
			if (cnt_wait++ > 60) break; //대기시간을 넘으면
			Sleep(1000); //1초씩 대기
		}


	}

	printf("info> terminated!\n");

	return 0;
}