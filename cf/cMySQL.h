#pragma once

#pragma comment(lib, "link/libmysql.lib")
#include <WinSock2.h>
#include "mysql/mysql.h"

/*
cMySQL: MySQL Control Class
@author: Vido
@Remark
- MySQL 접속과 쿼리생성, 결과 핸들링
- 클래스 생성후, Connect() 함수로 MySQL에 접속후 사용
*/
#define MAX_QUERY_BUFFER  65535
class cMySQL {
private:
	MYSQL	mMysql_handle;
	MYSQL_RES*      mSql_res;
	MYSQL_ROW       mSql_row;
	char mSql_ip[128];
	unsigned int mSql_port;
	char mSql_id[128];
	char mSql_pw[128];
	char mSql_db[128];
	char mQuery[MAX_QUERY_BUFFER];

public:
	cMySQL();
	~cMySQL();
	bool Connect(const char *sql_ip, unsigned int sql_port, const char *sql_id, const char *sql_pw, const char *sql_db);
	int Query(const char *format, ...);
	int FetchRow(void);
	char* GetStr(int num_col);
	int GetInt(int num_col);
	unsigned int GetUint(int num_col);
	double GetDouble(int num_col);
	unsigned int NumRow(void);
	void DeleteResult();


};