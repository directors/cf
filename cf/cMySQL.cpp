#include "cMySQL.h"
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#define LOWER(c)   (((c)>='A'  && (c) <= 'Z') ? ((c)+('a'-'A')) : (c))
#define UPPER(c)   (((c)>='a'  && (c) <= 'z') ? ((c)+('A'-'a')) : (c) )
#define atoui(x) strtoul((x), NULL, 10)

int strncasecmp(const char *arg1, const char *arg2, size_t n)
{
	int chk, i;

	if (arg1 == NULL || arg2 == NULL) {
		printf("strncasecmp(): received a NULL pointer, %p or %p.\n", arg1, arg2);
		return (0);
	}

	for (i = 0; (arg1[i] || arg2[i]) && (n > 0); i++, n--)
	if ((chk = LOWER(arg1[i]) - LOWER(arg2[i])) != 0)
		return (chk);	/* not equal */

	return (0);
}

cMySQL::cMySQL()
{
	
}

cMySQL::~cMySQL()
{
	mysql_close(&mMysql_handle);
}

bool cMySQL::Connect(const char *sql_ip, unsigned int sql_port, const char *sql_id, const char *sql_pw, const char *sql_db)
{
	mSql_res = NULL;
	mSql_row = NULL;
	strcpy_s(mSql_ip, 128, sql_ip);
	mSql_port = sql_port;
	strcpy_s(mSql_id, 128, sql_id);
	strcpy_s(mSql_pw, 128, sql_pw);
	strcpy_s(mSql_db, 128, sql_db);
	memset(mQuery, 0, MAX_QUERY_BUFFER);
	mysql_init(&mMysql_handle);

	//추가함
	mysql_options(&mMysql_handle, MYSQL_OPT_LOCAL_INFILE, 0);

	if (!mysql_real_connect(&mMysql_handle, sql_ip, mSql_id, mSql_pw, mSql_db, mSql_port, (char*)NULL, 0)) {
		//printf("Faild to connect MySQL DB..\n");
		return false;
	}
	//printf("[cMySQL] Connected To MySQL DB\n");

	//-- 문자셋 이거 안하면 한글이 깨짐;
	Query("set names euckr");

	return true;
}

int cMySQL::Query(const char *format, ...) {
	va_list args;

	if (format == NULL || *format == '\0') return 0;

	if (strncasecmp(mQuery, "SELECT", 6) == 0 ||
		strncasecmp(mQuery, "OPTIMIZE", 8) == 0)
		DeleteResult();

	va_start(args, format);
	//vsprintf(mQuery, format, args);
	vsprintf_s(mQuery, MAX_QUERY_BUFFER, format, args);
	va_end(args);

	if (mysql_query(&mMysql_handle, mQuery)) {
		printf("SQL_ERR: %s\n Req: %s\n", mysql_error(&mMysql_handle), mQuery);
		return -1;
	}

	if (strncasecmp(mQuery, "SELECT", 6) == 0 ||
		strncasecmp(mQuery, "OPTIMIZE", 8) == 0)
		mSql_res = mysql_store_result(&mMysql_handle);
	
	return 0;
}

int cMySQL::FetchRow(void) {
	if (!mSql_res) return 0;

	mSql_row = mysql_fetch_row(mSql_res);

	if (!mSql_row) return -1;

	return 0;
}

char* cMySQL::GetStr(int num_col) {
	char *result = NULL;
	if (!mSql_res) return NULL;

	if (!mSql_row) {
		printf("SQL_ERR: Access to Null Sql Row\n Col: %d, Req:%s\n", num_col, mQuery);
		return NULL;
	}

	if (mSql_row[num_col]) {
		result = mSql_row[num_col];
	}

	return result;
}

int cMySQL::GetInt(int num_col) {
	int result = 0;
	if (!mSql_res) return -1;

	if (!mSql_row) {
		printf("SQL_ERR: Access to Null Sql Row\n Col: %d, Req:%s\n", num_col, mQuery);
		return -1;
	}

	if (mSql_row[num_col]) {
		result = atoi(mSql_row[num_col]);
	}

	return result;
}

unsigned int cMySQL::GetUint(int num_col) {
	unsigned int result = 0;
	if (!mSql_res) return -1;

	if (!mSql_row) {
		printf("SQL_ERR: Access to Null Sql Row\n Col: %d, Req:%s\n", num_col, mQuery);
		return -1;
	}

	if (mSql_row[num_col]) {
		result = atoui(mSql_row[num_col]);
	}

	return result;
}

double cMySQL::GetDouble(int num_col) {
	double result = 0;
	if (!mSql_res) return -1;

	if (!mSql_row) {
		printf("SQL_ERR: Access to Null Sql Row\n Col: %d, Req:%s\n", num_col, mQuery);
		return -1;
	}

	if (mSql_row[num_col]) {
		result = atof(mSql_row[num_col]);
	}

	return result;
}

unsigned int cMySQL::NumRow(void) {
	if (mSql_res) return (unsigned int)mysql_num_rows(mSql_res);
	return 0;
}

void cMySQL::DeleteResult() {
	if (mSql_res) {
		mysql_free_result(mSql_res);
		mSql_res = NULL;
		mSql_row = NULL;
	}
}